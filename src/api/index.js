const express = require('express');

const { errorHandler } = require('../middleware');

// list of controllers here
const witnesses = require('../controllers/witnesses')
const proposals = require('../controllers/proposals')
const phishingAccounts = require('../controllers/phishingAccounts')

const witnessesV2 = require('../controllers/v2/witnesses')
const proposalsV2 = require('../controllers/v2/proposals')

const routersInit = (config, models) => {
    const router = express();

    // register api points
    router.use('/witnesses', witnesses(models, { config }));
    router.use('/proposals', proposals(models, { config }));
    router.use('/phishingAccounts', phishingAccounts(models, { config }));

    router.use('/v2/witnesses', witnessesV2(models, { config }));
    router.use('/v2/proposals', proposalsV2(models, { config }));

    // catch all api errors
    router.use(errorHandler);

    return router;
};

module.exports = routersInit;