const Client = require('lightrpc');
const bluebird = require('bluebird');
const client = new Client(process.env.BLURT_RPC_URL || 'http://localhost:8091');
bluebird.promisifyAll(client);

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const getBlock = blockNum => client.sendAsync({ method: 'block_api.get_block', params: {"block_num": blockNum}}, null);

const getOpsInBlock = (blockNum, onlyVirtual = false) =>
  client.sendAsync({ method: 'condenser_api.get_ops_in_block', params: [blockNum, onlyVirtual] }, null);

const getGlobalProps = () =>
  client.sendAsync({ method: 'condenser_api.get_dynamic_global_properties', params: [] }, null);

const mutliOpsInBlock = (start, limit, onlyVirtual = false) => {
  const request = [];
  for (let i = start; i < start + limit; i++) {
    request.push({ method: 'condenser_api.get_ops_in_block', params: [i, onlyVirtual] });
  }
  return client.sendBatchAsync(request, { timeout: 20000 });
};

const getBlockOps = block => {
  const operations = [];
  block.transactions.forEach(transaction => {
    operations.push(...transaction.operations);
  });
  return operations;
};

const FundedOption = {
  TOTALLY_FUNDED : 'totally_funded',
  PARTIALLY_FUNDED : 'partially_funded',
  NOT_FUNDED : 'not_funded',
}

const getProposalList = async (accountName) => {
  const result = await getClient().call('database_api', 'list_proposals', {
    start: [-1],
    limit: 1000,
    order: 'by_total_votes',
    order_direction: 'descending',
    status: 'votable',
  });

  const listProposalVotes = (
    await getClient().call('database_api', 'list_proposal_votes', {
      start: [accountName],
      limit: 1000,
      order: 'by_voter_proposal',
      order_direction: 'descending',
      status: 'votable',
    })
  ).proposal_votes
    .filter((item) => item.voter === accountName)
    .map((item) => item.proposal);

  let dailyBudget =
    parseFloat(
      (await getClient().database.getAccounts(['hive.fund']))[0].hbd_balance
        .toString()
        .split(' ')[0],
    ) / 100;
  return result.proposals.map((proposal) => {
    let fundedOption = FundedOption.NOT_FUNDED;
    if (dailyBudget > 0) {
      if (dailyBudget - parseFloat(proposal.daily_pay.amount) / 1000 >= 0) {
        fundedOption = FundedOption.TOTALLY_FUNDED;
      } else {
        fundedOption = FundedOption.PARTIALLY_FUNDED;
      }
    }

    dailyBudget = dailyBudget - parseFloat(proposal.daily_pay.amount) / 1000;
    return {
      id: proposal.id,
      creator: proposal.creator,
      proposalId: proposal.proposal_id,
      subject: proposal.subject,
      receiver: proposal.receiver,
      dailyPay: `${parseFloat(proposal.daily_pay.amount) / 1000} ${getSymbol(
        proposal.daily_pay.nai,
      )}`,
      link: `https://peakd.com/proposals/${proposal.proposal_id}`,
      startDate: moment(proposal.start_date),
      endDate: moment(proposal.end_date),
      totalVotes: `${nFormatter(
        toHP(
          (parseFloat(proposal.total_votes) / 1000000).toString(),
          (store.getState() as RootState).properties.globals,
        ),
        2,
      )} HP`,
      voted:
        listProposalVotes.find(
          (p: any) => p.proposal_id === proposal.proposal_id,
        ) !== undefined,
      funded: fundedOption,
    } as Proposal;
  });
};

module.exports = {
  sleep,
  getBlock,
  getOpsInBlock,
  getGlobalProps,
  mutliOpsInBlock,
  getBlockOps,
  getProposalList,
};
