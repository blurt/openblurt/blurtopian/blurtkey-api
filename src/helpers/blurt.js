const blurt = require('@blurtfoundation/blurtjs')
blurt.api.setOptions({ url: 'https://rpc.blurt.world', useAppbaseApi: true });

module.exports = blurt ;