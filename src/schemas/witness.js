const mongoose = require('mongoose');
const { Schema } = mongoose;

const WitnessSchema = new Schema({
  id: {
    type: String,
  },
  owner: {
    type: String,
  },
  created: {
    type: String,
  },
  url: {
    type: String,
  },
  votes: {
    type: String,
  },
});

module.exports = { WitnessSchema };