const mongoose = require('mongoose');
const { Schema } = mongoose;

const ProposalSchema = new Schema({
  creator: {
    type: String,
  },
  daily_pay: {
    type: String,
  },
  end_date: {
    type: String,
  },
  id: {
    type: String,
  },
  permlink: {
    type: String,
  },
  proposal_id: {
    type: String,
  },
  receiver: {
    type: String,
  },
  start_date: {
    type: String,
  },
  subject: {
    type: String,
  },
  total_votes: {
    type: String,
  },
});

module.exports = { ProposalSchema };