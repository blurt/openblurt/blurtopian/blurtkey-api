const { ProposalSchema } = require('./proposal');
const { WitnessSchema } = require('./witness');

module.exports = {
  ProposalSchema,
  WitnessSchema,
};