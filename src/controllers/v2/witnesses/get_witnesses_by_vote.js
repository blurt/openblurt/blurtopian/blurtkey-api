const blurt = require('../../../helpers/blurt')

const get_witnesses_by_vote = ({  }, { config }) => async (req, res, next) => {
  console.log('get_witnesses_by_vote v2')
  try {
    let from = null;
    let limit = 10;

    let result = await blurt.api.getWitnessesByVoteAsync(from, limit)
    res.status(200).send(result);
  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { get_witnesses_by_vote };