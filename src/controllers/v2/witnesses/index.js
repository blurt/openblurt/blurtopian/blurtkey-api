const { Router: router } = require('express');

const { get_witnesses_by_vote } = require('./get_witnesses_by_vote');

module.exports = (models, { config }) => {
  const api = router();

  api.get('/', get_witnesses_by_vote(models, { config }));

  return api;
};
