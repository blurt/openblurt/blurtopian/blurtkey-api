const { Router: router } = require('express');

const { list_proposals } = require('./list_proposals');

module.exports = (models, { config }) => {
  const api = router();

  api.get('/', list_proposals(models, { config }));

  return api;
};
