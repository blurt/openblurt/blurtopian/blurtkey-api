const blurt = require('../../helpers/blurt')
const phishing = require("@hiveio/hivescript/bad-actors.json");

const list_phishing_accounts = ({ ProjectOwner }, { config }) => async (req, res, next) => {
  try {
    let from = null;
    let limit = 100;

    let result = await blurt.api.getWitnessesByVoteAsync(from, limit)
    res.status(200).send(phishing);
  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { list_phishing_accounts };