const { Router: router } = require('express');

const { list_phishing_accounts } = require('./list_phishing_accounts');

module.exports = (models, { config }) => {
  const api = router();

  api.get('/', list_phishing_accounts(models, { config }));

  return api;
};
