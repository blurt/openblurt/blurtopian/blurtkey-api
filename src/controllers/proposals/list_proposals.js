const blurt = require('../../helpers/blurt')

const list_proposals = ({ ProjectOwner }, { config }) => async (req, res, next) => {
  try {
    let from = null;
    let limit = 100;

    let result = await blurt.api.getWitnessesByVoteAsync(from, limit)
    res.status(200).send({ data: result });
  } catch (error) {
    console.log('error', error)
    next(error);
  }
}

module.exports = { list_proposals };