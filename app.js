require('dotenv').config()
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const cors = require('cors');

const mongoose = require('mongoose');
const _ = require('lodash');

const api = require('./src/api');

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const {WitnessSchema, ProposalSchema} = require('./src/schemas');
const Witness = db1.model('Witness', WitnessSchema);
const Proposal = db1.model('Proposal', ProposalSchema);
const models = { Witness, Proposal };

// Express
const app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//CORS
app.use(cors());

// Routes
app.use('/blurt', api({}, models));

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;